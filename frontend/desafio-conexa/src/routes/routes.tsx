import React from 'react';
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import AtendmentRoom from '../pages/AtendmentRoom';
import Login from '../pages/Login';

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/login' element={<Login/>}/>
        <Route path='/atendment-room' element={<AtendmentRoom/>}/>
      </Routes>
    </BrowserRouter>
  );
}

export default Router;