import React from 'react';

import { 
  Content
 } from './styles';

interface IHeaderTitle {
  title?: string;
}

const HeaderTitle: React.FC<IHeaderTitle> = ({ title }) => {
  return (
    <Content>{title}</Content>
  );
}

export default HeaderTitle;