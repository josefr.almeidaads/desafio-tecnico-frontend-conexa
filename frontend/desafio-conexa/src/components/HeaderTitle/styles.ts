import styled from 'styled-components';

export const Content = styled.h1`
  color: ${props => props.theme.colors.blueDark};
  font-size: 2rem;
  font-family: 'Nunito';
  font-weight: 700;
  text-align: left;
`;