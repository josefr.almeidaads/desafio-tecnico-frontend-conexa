import styled from 'styled-components';

export const Content = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

export const WrapperForm = styled.div`
  display: flex;
  flex-direction: row;
  border-bottom: 1px solid ${props => props.theme.colors.grayMedium};
  margin-bottom: 2rem;
`;

export const Input = styled.input`
  display: flex;
  flex: 1;
  border: 0px;
  color: ${props => props.theme.colors.grayMedium};
  padding: 0 0.5rem 0.5rem 0;
`;

export const InputIcon = styled.img`
  color: ${props => props.theme.colors.grayMedium};
  width: 1rem;
  height: 1rem;
`;