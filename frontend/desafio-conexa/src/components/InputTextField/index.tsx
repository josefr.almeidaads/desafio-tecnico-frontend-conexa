import React, { useState } from 'react';
import { FaEye, FaEyeSlash } from 'react-icons/fa';

import {
  Content,
  WrapperForm,
  Input,
} from './styles';

import themes from '../../styles/themes';

interface ITextField {
  placeholder?: string;
  value?: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement> | undefined;
  type?: React.HTMLInputTypeAttribute | undefined;
  label?: string;
  secureTextEntry?: boolean;
}

const InputTextField: React.FC<ITextField> = ({
  onChange,
  value,
  type,
  secureTextEntry,
  placeholder,
  label,
}) => {
  const [showPassword, setShowPassword] = useState<boolean>(false);
  return (
    <Content>
      {label}
      <WrapperForm>
        <Input 
          placeholder={placeholder}
          type={!showPassword ? "text" : "password"}
          onChange={onChange}
          value={value}
        />
        {secureTextEntry && (showPassword ? <FaEye 
          color={themes.colors.grayMedium}
          onClick={() => setShowPassword(false)}
        /> :
        <FaEyeSlash 
          color={themes.colors.grayMedium}
          onClick={() => setShowPassword(true)}
        />)}
      </WrapperForm>
    </Content>
  );
}

export default InputTextField;

InputTextField.defaultProps = {
  label: '',
  placeholder: '',
  secureTextEntry: false,
  type: 'text',
  value: '',
  onChange: () => {}
}