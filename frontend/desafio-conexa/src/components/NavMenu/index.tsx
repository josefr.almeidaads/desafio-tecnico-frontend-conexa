import React from 'react';

import { 
  Content,
  Logo, 
} from './styles';

interface INavMenu {
  imageSrc: string;
}

const NavMenu: React.FC<INavMenu> = ({ imageSrc }) => {
  return (
    <Content>
      {imageSrc && <Logo src={imageSrc}/>}
    </Content>
  );
}

export default NavMenu;