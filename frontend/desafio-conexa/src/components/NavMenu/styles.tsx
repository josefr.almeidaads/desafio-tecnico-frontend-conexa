import styled from 'styled-components';

export const Content = styled.nav`
  @media(max-width: 783px){
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
    height: 4rem;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.25);
  }
  
  @media(min-width: 784px){
    display: flex;
    align-items: center;
    justify-content: flex-start;
    width: 100%;
    height: 4rem;
    box-shadow: 0px 0px 10px 0px rgba(0,0,0,0.25);
    padding-left: 1rem;
  }
`;

export const Logo = styled.img`
`;
