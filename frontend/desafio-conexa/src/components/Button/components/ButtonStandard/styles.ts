import styled from "styled-components";

export const ButtonField = styled.button`
  display: flex;
  width: 100%;
  height: 3rem;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.theme.colors.blue};
  border: none;
  border-radius: 8px;
  transition: 0.3s;

  &:hover {
    background-color: ${props => props.theme.colors.white};
    border: 0.15rem solid ${props => props.theme.colors.blue};
    text {
      color: ${props => props.theme.colors.blue};
    }
  }
`;

export const TextButton = styled.text`
  text-align: center;
  font-family: 'Nunito';
  font-weight: 700;
  color: ${props => props.theme.colors.white};
`;