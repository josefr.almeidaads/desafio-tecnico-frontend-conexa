import React from 'react';

import { 
  ButtonField,
  TextButton, 
} from './styles';

interface IButtonStandard {
  title?: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
}

const ButtonStandard: React.FC<IButtonStandard> = ({
  onClick,
  title,
}) => {
  return (
    <ButtonField onClick={onClick}>
      <TextButton>{title}</TextButton>
    </ButtonField>
  );
}

export default ButtonStandard;