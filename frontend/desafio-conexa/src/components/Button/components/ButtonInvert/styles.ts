import styled from "styled-components";

export const ButtonField = styled.button`
  display: flex;
  width: 20rem;
  height: 3rem;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.theme.colors.white};
  border: 0.15rem solid ${props => props.theme.colors.blue};
  border-radius: 8px;
  transition: 0.3s;

  &:hover {
    background-color: ${props => props.theme.colors.blue};
    border: none;
    text {
      color: ${props => props.theme.colors.white};
    }
  }

  @media(max-width: 780px){
    width: 100vw;
  }
`;

export const TextButton = styled.text`
  text-align: center;
  font-family: 'Nunito';
  font-weight: 700;
  color: ${props => props.theme.colors.blue};
`;