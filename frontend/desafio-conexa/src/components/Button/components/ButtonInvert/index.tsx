import React from 'react';

import { 
  ButtonField,
  TextButton, 
} from './styles';

interface IButtonInvert {
  title?: string;
  onClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
}

const ButtonInvert: React.FC<IButtonInvert> = ({
  onClick,
  title,
}) => {
  return (
    <ButtonField onClick={onClick}>
      <TextButton>{title}</TextButton>
    </ButtonField>
  );
}

export default ButtonInvert;