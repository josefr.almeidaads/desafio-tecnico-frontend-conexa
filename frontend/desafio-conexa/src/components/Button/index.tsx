import React from 'react';
import ButtonInvert from './components/ButtonInvert';
import ButtonStandard from './components/ButtonStandard';

interface IButton {
  title?: string;
  color?: 'standard' | 'invert' | undefined ;
  onClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
}

const Button: React.FC<IButton> = ({ title, color, onClick }) => {
  const types: {[key: string]: any} = {
    "standard": <ButtonStandard onClick={onClick} title={title}/>,
    "invert": <ButtonInvert onClick={onClick} title={title}/>
  }
  return (
    <>
      {(types[color as string])}
    </>
  );
}

export default Button;

Button.defaultProps = {
  title: '',
  color: 'standard',
}