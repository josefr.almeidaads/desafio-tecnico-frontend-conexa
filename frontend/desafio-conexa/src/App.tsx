import React from 'react';
import { ThemeProvider } from 'styled-components';

import themes from './styles/themes';
import GlobalTheme from './styles/global';
import Router from './routes/routes';

function App() {
  return (
    <ThemeProvider theme={themes}>
      <GlobalTheme />
      <Router />
    </ThemeProvider>
  );
}

export default App;
