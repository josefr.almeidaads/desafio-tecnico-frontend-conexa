import 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    colors: {
      blue: string;
      blueDark: string;
      grayMedium: string;
      grayDark: string;
      white: string;
      backgroundOffWhite: string;
    }
  }
}