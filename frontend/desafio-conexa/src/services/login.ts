import api from './api';

export const serviceLogin = async(email: string, password: string): Promise<void> => {
  try{
    const response = await api.post('/login', { email, password });
    console.log('Response ->', response.data);
    alert("Login Realizado com Sucesso!")
    return response.data;
  }catch(e: any){
    alert('Ocorreu um erro')
  }
}