import React, { useState } from 'react';
import { useNavigate, useRoutes } from 'react-router-dom';

import { 
  Content,
  WrapperInput 
} from './styles';
import Button from '../../../../components/Button';
import InputTextField from '../../../../components/InputTextField';
import { serviceLogin } from '../../../../services/login';

const SectionB: React.FC = () => {
  const navigate = useNavigate();
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const storeLogin = async() => {
    const response = await serviceLogin(email, password);
    await localStorage.setItem('@token:', response.token);
    
    if(Object.keys(response).length > 0){
      navigate('/atendment-room');
    }
    return;
  }
  return (
    <Content>
      <WrapperInput>
        <InputTextField 
          label='E-mail'
          placeholder='Digite seu e-mail'
          type="text"
          secureTextEntry={false}
          value={email}
          onChange={(e: any) => setEmail(e.target.value)}
        />
        <InputTextField 
          label='Senha'
          placeholder='Digite sua senha'
          type="password"
          secureTextEntry={true}
          value={password}
          onChange={(e:any) => setPassword(e.target.value)}
        />

        <Button 
          title="Entrar"
          color="standard"
          onClick={() => storeLogin()}
        />
      </WrapperInput>
    </Content>
  );
}

export default SectionB;