import styled from 'styled-components';

export const Content = styled.section`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const WrapperInput = styled.div`

  @media(min-width: 782px){
    display: flex;
    width: 20rem;
    flex-direction: column;
  }

  @media(max-width: 783px){
    display: flex;
    width: 100vw;
    flex-direction: column;
    padding: 0 2rem;
  }
`;