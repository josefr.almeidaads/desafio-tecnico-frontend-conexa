import styled from 'styled-components';

export const Content = styled.section`
  display: flex;
  flex: 1;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 50vh;

  @media(max-width: 783px){
    img {
      display: none;
    }
  }
`;

export const Title = styled.text`
  margin-bottom: 4rem;
  color: ${props => props.theme.colors.blueDark};
  font-family: 'Montserrat';
  font-size: 4rem;
`;

export const LogoFrame = styled.img`
 width: 30rem;
 height: 30rem;
`;