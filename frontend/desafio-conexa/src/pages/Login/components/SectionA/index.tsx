import React from 'react';

import { 
  Content,
  Title,
  LogoFrame, 
} from './styles';

import Logo from '../../../../assets/frame-A.svg';

const SectionA: React.FC = () => {
  return (
    <Content>
      <Title>Faça Login</Title>
      <LogoFrame src={Logo}/>
    </Content>
  );
}

export default SectionA;