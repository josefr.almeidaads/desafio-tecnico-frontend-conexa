import React from 'react';
import NavMenu from '../../components/NavMenu';

import { 
  Content,
  Main, 
} from './styles';

import logoConexa from '../../assets/logo-conexa.svg';
import SectionA from './components/SectionA';
import SectionB from './components/SectionB';

const Login: React.FC = () => {
  return (
    <Main>
      <NavMenu imageSrc={logoConexa}/>
      <Content>
        <SectionA/>
        <SectionB/>
      </Content>
    </Main>
  );
}

export default Login;