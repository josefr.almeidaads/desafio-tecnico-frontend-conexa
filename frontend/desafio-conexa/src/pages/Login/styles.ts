import styled from 'styled-components';

export const Main = styled.div`
  background-color: ${props => props.theme.colors.white};
`;

export const Content = styled.div`
  display: flex;
  flex: 1;
  flex-direction: row;
  padding: 8rem 0rem;
  align-items: center;
  justify-content: center;

  @media(max-width: 783px){
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
`;
