import styled from "styled-components";

export const Content = styled.section`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 1rem;
  height: 47.4vw;
  position: relative;

  button {
    width: 5rem;
  }

  @media(max-width: 783px){
    display: none;
  }

  button {
    position: absolute;
    top: 88vh;
    bottom: 0;
    right: 0;
    left: 1vw;
  }
`;
