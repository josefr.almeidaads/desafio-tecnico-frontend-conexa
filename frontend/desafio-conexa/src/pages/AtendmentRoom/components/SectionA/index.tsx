import React from 'react';
import Button from '../../../../components/Button';
import HeaderTitle from '../../../../components/HeaderTitle';

import { 
  Content, 
} from './styles';

const SectionA: React.FC = () => {
  return (
    <Content>
      <HeaderTitle title='Consultas'/>
      <Button 
        color='invert'
        title='Ajuda'
      />
    </Content>
  );
}

export default SectionA;