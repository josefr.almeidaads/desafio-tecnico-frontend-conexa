import styled from "styled-components";

export const Content = styled.footer`
  display: flex;
  flex: 1;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 6rem;
  border-top: 1px solid rgba(0,0,0,0.25);
  position: absolute;
  top: 93vh;
  bottom: 0;
  left: 0;
  right: 0;
  padding: 0 1rem 1rem 1rem;

  @media(min-width: 784px){
    display: none;
  }

  button {
    width: 6rem;
  }

  button + button {
    width: 15rem;
  }
`;