import React from 'react';
import Button from '../../../../components/Button';

import { 
  Content,
 } from './styles';

const Footer: React.FC = () => {
  return (
    <Content>
      <Button color='invert' title='Ajuda'/>
      <Button color='standard' title='Agendar consulta'/>
    </Content>
  );  
}

export default Footer;