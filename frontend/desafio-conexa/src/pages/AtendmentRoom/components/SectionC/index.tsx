import React from 'react';
import Button from '../../../../components/Button';

import { 
  Content
 } from './styles';

const SectionC: React.FC = () => {
  return (
    <Content>
      <Button 
        color='standard'
        title='Agenda consulta'
      />
    </Content>
  );
}

export default SectionC;