import styled from "styled-components";

export const Content = styled.section`
  display: flex;
  flex: 1;
  flex-direction: row-reverse;
  height: 47.5vw;
  position: relative;

  @media(max-width: 783px){
    display: none;
  }

  button {
    width: 15rem;
    position: absolute;
    top: 88vh;
    left: auto;
    right: 1vw;
    bottom: 0;
  }
`;