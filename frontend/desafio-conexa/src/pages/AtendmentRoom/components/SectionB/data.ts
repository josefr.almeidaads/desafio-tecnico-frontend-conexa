export interface IAtendments  {
  id: number;
  name: string;
  data: string;
}

export const atendments: IAtendments[] = [
  {
    id: 1,
    name: 'Pedro Marinho dos Santos',
    data: '20/06/2021 ás 8:00' 
  },
  {
    id: 2,
    name: 'Maria Joana da Mata',
    data: '20/06/2021 ás 9:00' 
  },
  {
    id: 3,
    name: 'Letícia Spinozza',
    data: '20/06/2021 ás 10:00' 
  },
]