import styled, { css } from "styled-components";

interface IContent {
  foundData: boolean;
}

export const Content = styled.section<IContent>`
  ${props => props.foundData ? css`
    display: flex;
    flex: 1;
    align-items: center;
    flex-direction: column;
    padding: 1rem;
    height: 93.5vh;

    h1 {
      height: 20%;
    }
  ` 
  : 
  css`
    display: flex;
    flex: 1;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    padding: 1rem;
    height: 93.5vh;
  `};

  h1 {
    display: flex;
    justify-content: flex-start;
  }

  h2 {

  }

  @media(min-width: 784px){
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    p {
      display: flex;
      justify-content: center;
      align-items: center;
      width: 15rem;
    }
    h2 {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    h1 {
      display: none;
    }
  }
  
`;

export const NotFoundData = styled.h2`
  font-family: 'Nunito';
  font-size: 1rem;
  font-weight: 700;
  color: ${props => props.theme.colors.grayMedium};
  width: 10rem;
  text-align: center;
`;

export const ListNumberOfAtendments = styled.p`
  font-family: 'Nunito';
  font-size: 1rem;
  font-weight: 700;
  color: ${props => props.theme.colors.grayDark};
  width: 11rem;
  text-align: center;
`;
