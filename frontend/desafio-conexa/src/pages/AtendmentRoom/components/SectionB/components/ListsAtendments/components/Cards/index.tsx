import React from 'react';
import Button from '../../../../../../../../components/Button';
import { 
  Content, 
  WrapCardButton, 
  WrapCardText, 
  WrapCardTextData, 
  WrapCardTextName} from './styles';

interface ICard {
  atendment: {
    id: number;
    name: string;
    data: string;
  }
}

const Cards: React.FC<ICard> = ({ atendment }) => {
  return (
    <Content>
      <WrapCardText>
        <WrapCardTextName>{atendment.name}</WrapCardTextName>
        <WrapCardTextData>{atendment.data}</WrapCardTextData>
      </WrapCardText>
      <WrapCardButton>
        <Button 
          color='standard'
          title='Atender'
        />
      </WrapCardButton>
    </Content>
  );
}

export default Cards;