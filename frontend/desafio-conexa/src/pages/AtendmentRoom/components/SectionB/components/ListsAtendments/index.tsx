import React from 'react';
import { IAtendments } from '../../data';
import Cards from './components/Cards';

import {
  ListAtendments,
  Atendment,
 } from './styles';

interface IListsAtendments {
  atendments: IAtendments[];
}

const ListsAtendments: React.FC<IListsAtendments> = ({ atendments }) => {
  return (
    <ListAtendments>
     {atendments?.map((atendment, index) => (
      <Cards key={atendment.id} atendment={atendment}/>
     ))}
    </ListAtendments>
  );
}

export default ListsAtendments;