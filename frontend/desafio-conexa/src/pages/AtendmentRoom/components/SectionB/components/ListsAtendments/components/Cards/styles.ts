import styled from "styled-components";

export const Content = styled.div`
  display: flex;
  flex: 1;
  height: 4rem;
  align-items: center;
  justify-content: space-between;
  width: 50vw;
  align-self: center;
  padding-left: 10vw;
  padding-top: 1vw;

  @media(max-width: 784px){
    width: 90vw;
    padding-left: 20vw;
    padding-top: 2vw;
  }
`;

export const WrapCardText = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`;

export const WrapCardTextName = styled.div`
  display: flex;
  flex: 1;
`;
export const WrapCardTextData = styled.div`
  display: flex;
  flex: 1;
`;

export const WrapCardButton = styled.div`
  display: flex;
  flex: 1;

  button {
    width: 10rem;
  }
`;