import React from 'react';
import HeaderTitle from '../../../../components/HeaderTitle';
import ListsAtendments from './components/ListsAtendments';

import {atendments} from './data';

import { 
  Content,
  NotFoundData,
  ListNumberOfAtendments,
} from './styles';

const SectionB: React.FC = () => {
  return (
    <Content foundData={atendments.length > 0}>
      <HeaderTitle title='Consultas'/>
       {atendments.length < 0 ? (<NotFoundData>
          Não há nenhuma consulta agendada
        </NotFoundData>) :
        (<ListNumberOfAtendments>
          {atendments.length} consultas agendadas.
        </ListNumberOfAtendments>
        )}
        {atendments.length > 0 && <ListsAtendments atendments={atendments}/>}
    </Content>
  );
}

export default SectionB;