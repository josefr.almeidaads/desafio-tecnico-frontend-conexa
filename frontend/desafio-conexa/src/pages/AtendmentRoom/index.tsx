import React from 'react';
import NavMenu from '../../components/NavMenu';

import { 
  Content,
  WrappSections, 
} from './styles';

import logo from '../../assets/logo-conexa.svg';

import SectionB from './components/SectionB';
import SectionC from './components/SectionC';
import SectionA from './components/SectionA';
import Footer from './components/Footer';

const AtendmentRoom: React.FC = () => {
  return (
    <Content>
      <NavMenu imageSrc={logo}/>
      <WrappSections>
        <SectionA />
        <SectionB />
        <SectionC />
      </WrappSections>
      <Footer />
    </Content>
  );
}

export default AtendmentRoom;