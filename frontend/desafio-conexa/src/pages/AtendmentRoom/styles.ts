import styled from 'styled-components';

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  position: relative;
`;

export const WrappSections = styled.div`
  display: flex;
  flex: 1;
`;