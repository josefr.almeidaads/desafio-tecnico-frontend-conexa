import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    outline: 0;
  }

  @media(max-width: 1366px){
    html {
      font-size: 93.75%; // (15px * 100%) / (16px) = 93.75% // 1rem(16px) -> 1rem(15px)
    }
  }

  @media(max-width: 320px){
    font-size: 87.5%; // (14px * 100%)/(16px) = 87.5% // 1rem(16px) -> 1rem(14px)
  }

  body {
    -webkit-font-smoothing: antialiased;
    font-family: 'Nunito';
    background-color: ${props => props.theme.colors.white};
  }

  a {
    text-decoration: none;
    color: inherit;
  }

  button {
    cursor: pointer;
  }
`;