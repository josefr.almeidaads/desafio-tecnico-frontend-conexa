export default {
  colors: {
    blue: '#2E50D4',
    blueDark: '#1C307F',
    grayMedium: '#999392',
    grayDark: '#575453',
    white: '#FFFFFF',
    backgroundOffWhite: '#FFFFFB',
  }
}